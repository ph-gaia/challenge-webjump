<?php

/**
 * Interface de Controller
 */

namespace Core\Interfaces;

interface ControllerInterface
{
    public function __construct(array $parametros);
    public function index();
}
