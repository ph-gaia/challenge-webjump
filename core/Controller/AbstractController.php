<?php

namespace Core\Controller;
use App\Config\ConfigApp as cfg;

class AbstractController
{

    protected $view;
    protected $pagina;
    protected $parametros = array();

    public function __construct(array $parametros)
    {
        $this->parametros = $parametros;
        $this->view = new \stdClass();
    }

    /**
     * Renderiza a página
     * 
     * @param string $pagina Nome do Arquivo a ser renderizado
     * @param boolean $useLayout Uso de outro Layout
     * @param string $alternativeLayout Nome do Layout alternativo
     */
    protected function render($pagina, $useLayout = true, $alternativeLayout = 'default')
    {

        $this->pagina = $pagina;
        $fileLayout = getcwd() . cfg::VIEWS_DIR . "Layout/{$alternativeLayout}.php";

        if ($useLayout == true && file_exists($fileLayout)) {
            include_once "$fileLayout";
        } else {
            echo $this->content();
        }
    }

    /**
     * Conteúdo da página
     */
    protected function content()
    {

        $classAtual = get_class($this);
        $controller = strtolower(str_replace("App\\Controllers\\", "", $classAtual));
        $singleClassName = str_replace("controller", "", $controller);
        $filename = getcwd() . cfg::VIEWS_DIR . ucfirst($singleClassName) . '/' . $this->pagina . '.php';        

        if (!file_exists($filename)) {
            echo "Arquivo não existe!";
            exit;
        }

        include_once $filename;
    }

    /**
     * Retorna os parametros passados na URI
     * @param string $indice Nome do parametro a ser retornado
     * @return mixed
     */
    protected function getParams($indice = null)
    {
        if (array_key_exists($indice, $this->parametros)) {
            return $this->parametros[$indice];
        }
        return null;
    }
}
