<?php

/**
 * @file Mensagem.php
 * - Helper responsavel por printar na tela as mensagens de diálogo com o usuário
 */

namespace App\Helpers;

class Mensagem
{

    private static $msg;

    private static function setMsgDefault()
    {
        self::$msg = [
            '000' => 'Erro ao executar operação.',
            '111' => 'Sucesso ao executar operação.'
        ];
    }

    public static function showMsg($message, $tipo, $exit = true)
    {
        self::setMsgDefault();
        $msg = '';

        if (isset(self::$msg[$message])) {
            $msg = "<span class='closebtn' onclick='this.parentElement.style.display='none';'>&times;</span>" . self::$msg[$message];
        } else {
            $msg = "<span class='closebtn' onclick='this.parentElement.style.display='none';'>&times;</span>" . $message;
        }
        echo "<div class='alert {$tipo}'>{$msg}</div>";

        if ($exit) {
            exit;
        }
    }
}
