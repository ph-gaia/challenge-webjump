<?php

namespace App\Helpers;

class Logger
{

    const DS = DIRECTORY_SEPARATOR;

    public function __construct()
    {
        $this->createFile();
    }

    public static function info($message)
    {
        self::addRecord(self::validateMessage($message, "INFO"));
    }

    public static function warning($message)
    {
        self::addRecord(self::validateMessage($message, "WARNING"));
    }

    public static function error($message)
    {
        self::addRecord(self::validateMessage($message, "ERROR"));
    }

    private static function validateMessage($message, $level)
    {
        if (empty($message)) {
            return false;
        }

        $data = "";
        if (!empty($params)) {
            $data = join(", ");
            $message . $data;
        }

        $date = date('Y-m-d H:i:s');

        $msg = sprintf("[%s] [%s]: %s %s", $date, $level, $message, PHP_EOL);

        return $msg;
    }

    private static function addRecord($message)
    {
        $filename = "/home/phenrique/Projects/challenge-webjump/App/Logs/webjump.txt";

        file_put_contents($filename, $message, FILE_APPEND);
    }
}
