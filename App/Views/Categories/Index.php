<div class="header-list-page">
    <h1 class="title">Categories</h1>
    <a href="/categories/create" class="btn-action">Add new Category</a>
</div>
<table class="data-grid">
    <tr class="data-row">
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Name</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Code</span>
        </th>
        <th class="data-grid-th">
            <span class="data-grid-cell-content">Actions</span>
        </th>
    </tr>
    <?php foreach ($this->view->result as $value) : ?>
        <tr class="data-row">
            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?= $value['name'] ?></span>
            </td>

            <td class="data-grid-td">
                <span class="data-grid-cell-content"><?= $value['code'] ?></span>
            </td>

            <td class="data-grid-td">
                <div class="actions">
                    <a style="cursor: pointer;" onclick="confirmar('Deseja editar este registro?', '/categories/edit/<?= $value['id']; ?>')">
                        <span>Edit</span>
                    </a>
                    <a onclick="confirmar('Deseja excluir este registro?', '/categories/delete/id/<?= $value['id']; ?>')">
                        <span>Delete</span>
                    </a>
                </div>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
<div class="pagination">
    <a href="/categories/visualizar/page/<?= $this->view->btn['previous']; ?>" aria-label="Previous">
        <span aria-hidden="true">&laquo;</span>
    </a>
    <?php foreach ($this->view->btn['link'] as $value) : ?>
        <a href="/categories/visualizar/page/<?= $value; ?>"><?= $value; ?></a>
    <?php endforeach; ?>
    <a href="/categories/visualizar/page/<?= $this->view->btn['next']; ?>" aria-label="Next">
        <span aria-hidden="true">&raquo;</span>
    </a>
</div>