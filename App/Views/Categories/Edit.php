<h1 class="title">Edit Category</h1>

<div id="resultado"></div>
<form action="/categories/update" id="form">
    <input type="hidden" name="id" value="<?= $this->view->result['id'] ?>">
    <div class="input-field">
        <label for="category-name" class="label">Category Name</label>
        <input type="text" id="category-name" name="name" value="<?= $this->view->result['name'] ?>" class="input-text" />
    </div>
    <div class="input-field">
        <label for="category-code" class="label">Category Code</label>
        <input type="text" id="category-code" name="code" value="<?= $this->view->result['code'] ?>" class="input-text" />
    </div>
    <div class="actions-form">
        <a href="/categories" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save" />
    </div>
</form>