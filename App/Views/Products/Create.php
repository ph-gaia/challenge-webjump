<h1 class="title new-item">New Product</h1>

<div id="resultado"></div>
<form action="/products/register" id="form">
    <div class="input-field">
        <label for="sku" class="label">Product SKU</label>
        <input type="text" id="sku" name="sku" class="input-text" />
    </div>
    <div class="input-field">
        <label for="name" class="label">Product Name</label>
        <input type="text" id="name" name="name" class="input-text" />
    </div>
    <div class="input-field">
        <label for="price" class="label">Price</label>
        <input type="text" id="price" name="price" class="input-text" />
    </div>
    <div class="input-field">
        <label for="quantity" class="label">Quantity</label>
        <input type="text" id="quantity" name="quantity" class="input-text" />
    </div>
    <div class="input-field">
        <label for="category" class="label">Categories</label>
        <select multiple id="category" name="category[]" class="input-text">
            <?php foreach ($this->view->categorias as $value) : ?>
                <option value="<?= $value['id']; ?>"><?= $value['name'] ?></option>
            <?php endforeach; ?>
        </select>
    </div>
    <div class="input-field">
        <label for="description" class="label">Description</label>
        <textarea id="description" name="description" class="input-text"></textarea>
    </div>
    <div class="actions-form">
        <a href="/products" class="action back">Back</a>
        <input class="btn-submit btn-action" type="submit" value="Save Product" />
    </div>

</form>