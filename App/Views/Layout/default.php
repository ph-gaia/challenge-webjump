<!doctype html>
<html ⚡>

<head>
    <title><?= $this->view->title ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,minimum-scale=1">

    <?php include_once 'Common/styles.php'; ?>
</head>

<body>
    <?php include_once 'Partials/menu_horizontal_top.php'; ?>
    <!-- Main Content -->
    <main class="content">

        <?= $this->content(); ?>

    </main>
    <!-- Main Content -->
    <?php include_once 'Partials/footer.php'; ?>

    <?php include_once 'Common/scripts.php'; ?>
</body>

</html>