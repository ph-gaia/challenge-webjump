<script src="https://code.jquery.com/jquery-3.4.1.min.js" integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo=" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>
<script async src="https://cdn.ampproject.org/v0.js"></script>
<script async custom-element="amp-fit-text" src="https://cdn.ampproject.org/v0/amp-fit-text-0.1.js"></script>
<script async custom-element="amp-sidebar" src="https://cdn.ampproject.org/v0/amp-sidebar-0.1.js"></script>

<script type="text/javascript">
    function confirmar(title, url) {
        if (confirm(title)) {
            window.location = url;
        }
    }
    $(document).ready(function() {

        $('#form').submit(function() {
            $('#resultado').html("Enviando...");

            var dados = $(this).serialize();
            $.ajax({
                type: "POST", // Tipo de metodo
                url: $(this).attr("action"), //Recebe o valor da action do form
                data: dados,
                success: function(data) //Se tiver sucesso...
                {
                    $("#resultado").html(data);
                }
            });
            return false;
        });
    });
</script>