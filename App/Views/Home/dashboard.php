<?php

use App\Helpers\View;

?>
<div class="header-list-page">
    <h1 class="title">Dashboard</h1>
</div>
<div class="infor">
    You have <?= count($this->view->result) ?> products added on this store: <a href="/products/create" class="btn-action">Add new Product</a>
</div>
<ul class="product-list">
    <?php foreach ($this->view->result as $value) : ?>
        <li>
            <div class="product-image">
                <img src="../../../assets/images/product/tenis-runner-bolt.png" layout="responsive" width="164" height="145" alt="Tênis Runner Bolt" />
            </div>
            <div class="product-info">
                <div class="product-name"><span><?= View::limitString($value['name']) ?></span></div>
                <div class="product-price"><span class="special-price">
                    <?= $value['quantity'] ?> available</span>
                    <span>R$ <?= number_format($value['price'], 2, ",", "."); ?></span>
                </div>
            </div>
        </li>
    <?php endforeach; ?>
</ul>