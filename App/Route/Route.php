<?php

/**
 * Class onde sao definidas as rotas do sistema
 */

namespace App\Route;

use App\Config\ConfigApp as cfg;
use Core\RouterSystem\RouteMap;

class Route
{

    /**
     * @var RouteMap
     */
    private $routeMap;

    public function __construct(RouteMap $routeMap)
    {
        $this->routeMap = $routeMap;
    }

    /**
     * Registra as rotas no sistma
     */
    public function registra()
    {
        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/', 'HomeController@index', array()));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/home', 'HomeController@index', array()));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/categories', 'CategoriesController@index', array()));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/categories/visualizar/page/{page}', 'CategoriesController@index', array(
            'page' => '/\d+/'
        )));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/categories/create', 'CategoriesController@create', array()));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/categories/edit/{id}', 'CategoriesController@edit', array(
            'id' => '/\d+/'
        )));

        $this->routeMap->rotaPost(array(cfg::PREFIX_ROUTE . '/categories/register', 'CategoriesController@register', array()));

        $this->routeMap->rotaPost(array(cfg::PREFIX_ROUTE . '/categories/update', 'CategoriesController@update', array()));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/categories/delete/id/{id}', 'CategoriesController@remove', array(
            'id' => '/\d+/'
        )));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/products', 'ProductsController@index', array()));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/products/visualizar/page/{page}', 'ProductsController@index', array(
            'page' => '/\d+/'
        )));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/products/create', 'ProductsController@create', array()));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/products/edit/{id}', 'ProductsController@edit', array(
            'id' => '/\d+/'
        )));

        $this->routeMap->rotaPost(array(cfg::PREFIX_ROUTE . '/products/register', 'ProductsController@register', array()));

        $this->routeMap->rotaPost(array(cfg::PREFIX_ROUTE . '/products/update', 'ProductsController@update', array()));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/products/create', 'ProductsController@create', array()));

        $this->routeMap->rotaGet(array(cfg::PREFIX_ROUTE . '/products/delete/id/{id}', 'ProductsController@remove', array(
            'id' => '/\d+/'
        )));
    }

    /**
     * Retorna a referencia para o objeto RouteMap
     * @return RouteMap
     */
    public function getRouteMap()
    {
        return $this->routeMap;
    }
}
