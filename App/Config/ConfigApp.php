<?php

/**
 * Configuraçao das costantes do sistema
 */
namespace App\Config;

class ConfigApp
{

    // diretorios de views
    const VIEWS_DIR = '/../App/Views/';
    // diretorios de views
    const PATH_LOG = '/../Logs/';
    // prefixo usado nas rotas
    const PREFIX_ROUTE = null;

}