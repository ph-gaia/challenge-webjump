<?php
/**
 * Configuraçao das credenciais de conexao com o Banco de Dados
 */
namespace App\Config;

class ConfigDatabase
{

    public $db = array(
        'sgbd' => 'mysql',
        'server' => 'localhost',
        'dbname' => 'webjump',
        'username' => 'webapp',
        'password' => 'webapp',
        'options' => array(\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"),
    );

}