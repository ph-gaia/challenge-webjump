<?php

/**
 * Model de Products
 */

namespace App\Models;

use Core\Database\ModelAbstract;
use App\Helpers\Paginator;
use Core\Http\Header;
use App\Helpers\Logger;
use App\Helpers\Mensagem;

class ProductsModel extends ModelAbstract
{

    private $header;

    private $entity = 'products';

    private $paginator;

    public function __construct()
    {
        parent::__construct();

        $this->header = new Header();
    }

    public function paginator($pagina)
    {
        $data = [
            'entidade' => $this->entity,
            'select' => '*',
            'pagina' => $pagina,
            'maxResult' => 5,
            'orderBy' => ''
        ];

        $this->paginator = new Paginator($data);
    }

    public function getResultPaginator()
    {
        return $this->paginator->getResultado();
    }

    public function getNavePaginator()
    {
        return $this->paginator->getNaveBtn();
    }

    public function findById($id)
    {
        $sql = "SELECT * FROM {$this->entity} WHERE id = ?";

        $stmt = $this->pdo()->prepare($sql);
        $stmt->execute([$id]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function register()
    {
        Logger::warning("Started creating a new product");
        $data = $this->validateAll();

        try {
            $sql = "INSERT INTO {$this->entity} (`name`, `sku`, `price`, `description`, `quantity`) ";
            $sql .= "VALUES ('" . $data['name'] . "','" . $data['sku'] . "'," . $data['price'] . ",'" . $data['description'] . "'," . $data['quantity'] . ");";

            $stmt = $this->pdo()->prepare($sql);
            $stmt->execute();
            $id = $this->pdo()->lastInsertId();

            foreach ($data['categories'] as $value) {
                $sql = "INSERT INTO `products_has_categories` (`products_id`, `categories_id`) VALUES ($id, $value);";
                $stmt = $this->pdo()->prepare($sql);
                $stmt->execute();
            }

            $this->header->setHttpHeader(201);
            Mensagem::showMsg("111", "success");
            Logger::info("New product created " . json_encode($data));
        } catch (\Exception $ex) {
            $this->header->setHttpHeader(400);
            Mensagem::showMsg("000", "danger");
            Logger::error("Error creating new product.");
        }
    }

    public function edit()
    {
        $data = $this->validateAll();

        try {
            $sql = "UPDATE {$this->entity} SET "
                . "name = '" . $data['name'] . "', "
                . "sku = '" . $data['sku'] . "', "
                . "price = " . $data['price'] . ","
                . "description = '" . $data['name'] . "',"
                . "quantity = " . $data['quantity'] . " "
                . "WHERE id = " . $data['id'] . "";

            $stmt = $this->pdo()->prepare($sql);

            if ($stmt->execute($data)) {
                $this->header->setHttpHeader(200);
                Mensagem::showMsg("111", "success");
                Logger::info("Updated Product " . json_encode($data));
            }
        } catch (\Exception $ex) {
            Mensagem::showMsg("000", "danger");
            $this->header->setHttpHeader(400);
            Logger::error("Error updating product.");
        }
    }

    public function remove($id)
    {
        try {
            $sqlRelationship = "DELETE FROM  products_has_categories WHERE products_id = ? ";
            $stmt1 = $this->pdo()->prepare($sqlRelationship);
            $stmt1->execute([$id]);

            $sql = "DELETE FROM  {$this->entity} WHERE id = ? ";
            $stmt = $this->pdo()->prepare($sql);

            if ($stmt->execute([$id])) {
                $this->header->setHttpHeader(200);
                Logger::info("Removed product");
                $this->contentTypeJSON()->toJson([
                    "status" => "success",
                    "message" => "Registry removed successfully"
                ]);
                return true;
            }
        } catch (\Exception $ex) {
            $this->header->setHttpHeader(500);
            Mensagem::showMsg("000", "danger");
            Logger::error("Error removing product.");
        }
    }

    private function validateAll()
    {
        $id = filter_input(INPUT_POST, "id");
        $name = filter_input(INPUT_POST, "name");
        $sku = filter_input(INPUT_POST, "sku");
        $price = filter_input(INPUT_POST, "price");
        $description = filter_input(INPUT_POST, "description");
        $quantity = filter_input(INPUT_POST, "quantity");
        $categories = isset($_POST["category"]) ?? [];

        if (!isset($name) || !isset($price) || !isset($sku)) {
            $this->header->setHttpHeader(400);
            $this->contentTypeJSON()->toJson([
                "status" => "error",
                "message" => "The field must be completed correctly"
            ]);
            exit;
        }

        return array(
            'id' => $id ? $id : null,
            'name' => $name,
            'sku' => $sku,
            'price' => $price,
            'description' => $description,
            'quantity' => $quantity,
            'categories' => $categories
        );
    }
}
