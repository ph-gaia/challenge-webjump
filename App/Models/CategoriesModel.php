<?php

/**
 * Model de Categories
 */

namespace App\Models;

use Core\Database\ModelAbstract;
use App\Helpers\Paginator;
use Core\Http\Header;
use App\Helpers\Logger;
use App\Helpers\Mensagem;

class CategoriesModel extends ModelAbstract
{

    private $header;

    private $entity = 'categories';

    private $paginator;

    public function __construct()
    {
        parent::__construct();

        $this->header = new Header();
    }

    public function paginator($pagina)
    {
        $data = [
            'entidade' => $this->entity,
            'select' => '*',
            'pagina' => $pagina,
            'maxResult' => 5,
            'orderBy' => ''
        ];

        $this->paginator = new Paginator($data);
    }

    public function getResultPaginator()
    {
        return $this->paginator->getResultado();
    }

    public function getNavePaginator()
    {
        return $this->paginator->getNaveBtn();
    }

    public function findAll()
    {
        $sql = "SELECT * FROM {$this->entity}";

        $stmt = $this->pdo()->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function findById($id)
    {
        $sql = "SELECT * FROM {$this->entity} WHERE id = ?";

        $stmt = $this->pdo()->prepare($sql);
        $stmt->execute([$id]);
        $result = $stmt->fetch(\PDO::FETCH_ASSOC);

        return $result;
    }

    public function register()
    {
        $data = $this->validateAll();

        try {
            $sql = "INSERT INTO {$this->entity} (`id`, `code`, `name`) VALUES (:id, :code, :name);";
            $stmt = $this->pdo()->prepare($sql);

            if ($stmt->execute($data)) {
                $this->header->setHttpHeader(201);
                Mensagem::showMsg("111", "success");
                Logger::info("New category created " . json_encode($data));
            }
        } catch (\Exception $ex) {
            $this->header->setHttpHeader(400);
            Mensagem::showMsg("000", "danger");
            Logger::error("Error creating new category.");
        }
    }

    public function edit()
    {
        $data = $this->validateAll();

        try {
            $sql = "UPDATE {$this->entity} SET "
                . "code = :code, "
                . "name = :name "
                . "WHERE id = :id";

            $stmt = $this->pdo()->prepare($sql);

            if ($stmt->execute($data)) {
                $this->header->setHttpHeader(200);
                Mensagem::showMsg("111", "success");
                Logger::info("Updated category " . json_encode($data));
            }
        } catch (\Exception $ex) {
            $this->header->setHttpHeader(400);
            Mensagem::showMsg("000", "danger");
            Logger::error("Error updating category.");
        }
    }

    public function remove($id)
    {
        try {
            $sqlRelationship = "DELETE FROM  products_has_categories WHERE categories_id = ?";
            $stmt1 = $this->pdo()->prepare($sqlRelationship);
            $stmt1->execute([$id]);

            $sql = "DELETE FROM  {$this->entity} WHERE id = ? ";
            $stmt = $this->pdo()->prepare($sql);

            if ($stmt->execute([$id])) {
                $this->header->setHttpHeader(200);
                Logger::info("Removed category");
                $this->contentTypeJSON()->toJson([
                    "status" => "success",
                    "message" => "Registry removed successfully"
                ]);
                return true;
            }
        } catch (\Exception $ex) {
            $this->header->setHttpHeader(500);
            Mensagem::showMsg("000", "danger");
            Logger::error("Error removing category.");
        }
    }

    private function validateAll()
    {

        $id = filter_input(INPUT_POST, "id");
        $name = filter_input(INPUT_POST, "name");
        $code = filter_input(INPUT_POST, "code");

        if (!isset($name)) {
            $this->header->setHttpHeader(400);
            $this->contentTypeJSON()->toJson([
                "status" => "error",
                "message" => "The name field must be completed correctly"
            ]);
            exit;
        }

        return array(
            ':id' => $id ? $id : null,
            ':code' => $code,
            ':name' => $name
        );
    }
}
