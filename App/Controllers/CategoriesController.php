<?php

namespace App\Controllers;

use Core\Controller\AbstractController;
use Core\Interfaces\ControllerInterface;
use App\Models\CategoriesModel;

class CategoriesController extends AbstractController implements ControllerInterface
{

    public function __construct(array $parametros)
    {
        parent::__construct($parametros);
    }

    public function index()
    {
        $this->view->title = "Categorias";
        $model = new CategoriesModel();
        $model->paginator($this->getParams("page"));
        $this->view->result = $model->getResultPaginator();
        $this->view->btn = $model->getNavePaginator();

        $this->render("Index");
    }

    public function create()
    {
        $this->view->title = "Criar Categorias";
        $this->render("Create");
    }

    public function edit()
    {
        $model = new CategoriesModel();
        $this->view->title = "Editar Categorias";
        $this->view->result = $model->findById($this->getParams('id'));
        $this->render("Edit");
    }

    public function register()
    {
        $model = new CategoriesModel();
        $model->register();
    }

    public function update()
    {
        $model = new CategoriesModel();
        $model->edit();
    }

    public function remove()
    {
        $id = $this->getParams('id');

        $model = new CategoriesModel();
        $model->remove($id);
    }
}
