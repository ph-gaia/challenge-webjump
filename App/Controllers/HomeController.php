<?php

namespace App\Controllers;

use Core\Controller\AbstractController;
use Core\Interfaces\ControllerInterface;
use App\Models\ProductsModel;

class HomeController extends AbstractController implements ControllerInterface
{

    public function __construct(array $parametros)
    {
        parent::__construct($parametros);
    }

    public function index()
    {
        $model = new ProductsModel();
        $this->view->title = "Home";

        $model->paginator($this->getParams("page"));
        $this->view->result = $model->getResultPaginator();
        $this->view->btn = $model->getNavePaginator();
        $this->render("dashboard");
    }
}
