<?php

namespace App\Controllers;

use Core\Controller\AbstractController;
use Core\Interfaces\ControllerInterface;
use App\Models\CategoriesModel;
use App\Models\ProductsModel;

class ProductsController extends AbstractController implements ControllerInterface
{

    public function __construct(array $parametros)
    {
        parent::__construct($parametros);
    }

    public function index()
    {
        $this->view->title = "Produtos";
        $model = new ProductsModel();

        $model->paginator($this->getParams("page"));
        $this->view->result = $model->getResultPaginator();
        $this->view->btn = $model->getNavePaginator();

        $this->render("Index");
    }

    public function create()
    {
        $this->view->title = "Criar Produtos";
        $model = new CategoriesModel();
        $this->view->categorias = $model->findAll();
        $this->render("Create");
    }

    public function edit()
    {
        $model = new ProductsModel();
        $categorias = new CategoriesModel();
        $this->view->title = "Editar Produtos";
        $this->view->result = $model->findById($this->getParams('id'));        
        $this->view->categorias = $categorias->findAll();
        $this->render("Edit");
    }

    public function register()
    {
        $model = new ProductsModel();
        $model->register();
    }

    public function update()
    {
        $model = new ProductsModel();
        $model->edit();
    }

    public function remove()
    {
        $id = $this->getParams('id');

        $model = new ProductsModel();
        $model->remove($id);
    }
}
