#!/usr/bin/php
<?php
require_once __DIR__ . '/vendor/autoload.php';

$import = new ImportAssistWebJump();
$db = new App\Config\ConfigDatabase();

if (isset($argv[1])) {
    // seta os parametros
    $import->setParams($argv);
    // executa os comandos
    $import->run();
} else {
    $import->welcome();
}


class ImportAssistWebJump
{

    private $connection;
    private $args = [];
    const PATH_DUMP_SQL = __DIR__ . '/public/dump.sql';
    const PATH_IMPORT = __DIR__ . '/public/assets/import.csv';
    const DS = DIRECTORY_SEPARATOR;

    public function __construct()
    {
        $this->args = [
            'import' => [
                'desc' => " Importa todos os registros do arquivo 'public/assets/import.csv'. ",
                'use' => '[ $ php jumper import ]'
            ],
            'setup' => [
                'desc' => " Cria o banco de dados da aplicação ",
                'use' => '[ $ php jumper setup ]'
            ],
            'help' => [
                'desc' => " Funcionalidades disponiveis ",
                'use' => '[ $ php jumper help ]'
            ]
        ];
    }

    public function run()
    {
        $command = $this->getParams();
        if ($this->verifyCommand($command[0])) {
            $this->connectDatabase();
            if ($command[0] == 'setup') {
                $this->createDataBase();
            } else if ($command[0] == 'help') {
                $this->showHelp();
            } else {
                $this->importCarga();
            }
        } else {
            // imprime a mensagem de erro | comando inválido
            $this->message("[ERRO]\n O comando \"{$command[0]}\" nao eh valido!\n"
                . "Para verificar os comandos validos consulte o\n"
                . "repositorio oficial ou execute o HELP:\n"
                . "[ \$ php jumper help ]");
            exit;
        }
    }

    /**
     * Return a array of comands valid
     * @return array
     */
    public function getCommands()
    {
        return $this->args;
    }

    /**
     * Set params parsed by prompt
     * @param array $value params values
     */
    public function setParams(array $value)
    {
        // Remove the first element array
        array_shift($value);

        $this->params = $value;
    }

    /**
     * Get Params
     * @return array
     */
    protected function getParams()
    {
        return $this->params;
    }

    /**
     * Verify if a command is valid
     * @param string $value command search
     * @return bool
     */
    protected function verifyCommand($value)
    {
        return array_key_exists($value, $this->args);
    }

    /**
     * Print a message on screen. Use just one line.
     * @param string $message
     */
    private function message($message)
    {
        echo $message . PHP_EOL;
    }

    /**
     * print a message welcome
     */
    public function welcome()
    {
        $this->message("Seja bem vindo ao assistente de importação Webjump. \n");
    }

    /**
     * Try creating the database schemas according the dump.sql file
     * @throws \Exception
     */
    private function createDataBase()
    {
        try {
            // load SQL Dump
            $sqlFile = $this->loadFile(self::PATH_DUMP_SQL);
            // connect into database and execute the SQL queries
            $this->connection->pdo()->exec($sqlFile);
            $this->message('> Banco de Dados criado com sucesso');
        } catch (\PDOException $ex) {
            throw new \Exception(""
                . "Não foi possível executar o dump.sql" . PHP_EOL
                . "Log:" . $ex->getMessage()
                . "" . PHP_EOL);
        }
    }

    /**
     * Try insert data
     * @throws \Exception
     */
    private function importCarga()
    {
        try {
            $this->message('> Importação de dados iniciada...');
            $data = $this->readFileCsv(self::PATH_IMPORT, ";");

            foreach ($data as $value) {
                $sql = "INSERT INTO `products` "
                    . " (`name`, `sku`, `description`, `quantity`, `price`) VALUES "
                    . " ('" . $value['name'] . "', '" . $value['sku'] . "', "
                    . " '" . $value['description'] . "', " . $value['quantity'] . ", " . $value['price'] . ");";

                $stmt = $this->connection->pdo()->prepare($sql);
                $stmt->execute();
                $id = $this->connection->pdo()->lastInsertId();

                $categories = $value['categories'];
                $category = explode("|", $categories);
                $categoriesId = $this->findCategoryOrCreate($category);

                foreach($categoriesId as $value) {
                    $sql = "INSERT INTO `products_has_categories` (`products_id`, `categories_id`) "
                    . " VALUES ($id, $value);";
                    $stmt = $this->connection->pdo()->prepare($sql);
                    $stmt->execute();
                }
            }

            $this->message('> Importação de dados finalizada com sucesso!');
        } catch (\Exception $ex) {
            throw new \Exception(""
                . "Não foi possível inserir os dados no sistema." . PHP_EOL
                . "Log:" . $ex->getMessage()
                . "" . PHP_EOL);
        }
    }

    private function findCategoryOrCreate($categories)
    {
        $categoriesId = [];
        foreach ($categories as $value) {
            $sql = " SELECT * FROM categories WHERE name LIKE '" . $value . "'";
            $stmt = $this->connection->pdo()->prepare($sql);
            $stmt->execute();
            $result = $stmt->fetch(\PDO::FETCH_ASSOC);

            if (isset($result['id'])) {
                $categoriesId[] = $result['id'];
                continue;
            }

            $sqlInsert = "INSERT INTO categories (`name`) VALUES ('$value');";
            $stmt = $this->connection->pdo()->prepare($sqlInsert);
            $stmt->execute();
            $id = $this->connection->pdo()->lastInsertId();
            $categoriesId[] = $id;
        }
        return $categoriesId;
    }

    /**
     * Try connect with database and returns the connection reference
     * @return \PDO
     * @throws \Exception
     */
    private function connectDatabase()
    {
        try {
            if (!$this->connection) {
                $this->connection = new Core\Database\ModelAbstract;
            }
            return $this->connection;
        } catch (\Exception $ex) {
            throw new \Exception(""
                . "ERRO!"
                . PHP_EOL
                . "Não foi possível connectar ao banco de dados"
                . PHP_EOL
                . $ex->getMessage()
                . PHP_EOL
                . "" . PHP_EOL);
        }
    }

    /**
     * Try read the file according path
     * @param string $path The path of file
     * @return string The file content
     * @throws \Exception
     */
    private function loadFile($path)
    {
        /**
         * Internal closure to throw a new Exception
         */
        $throwException = function () use ($path) {
            throw new \Exception(""
                . "ERRO!"
                . PHP_EOL
                . "Não foi possível ler arquivo:"
                . PHP_EOL
                . "Path: {$path}"
                . "" . PHP_EOL);
        };

        if (file_exists($path) || !is_readable($path)) {
            $fileContent = file_get_contents($path);
            if ($fileContent === false) {
                $throwException();
            }
            return $fileContent;
        } else {
            $throwException();
        }
    }

    /**
     * Função responsável por processar todas as linhas do arquivo CSV e retornar os dados em forma de Array
     * ---------------------------
     *
     * @param string $arquivo - arquivo contendo CSV
     * @param char $delimitador - caractere delimitador de colunas
     * @param char $enclosure - carácter de scape para strings
     * @return array - linhas do arquivo
     */
    public function readFileCsv($arquivo = null, $delimitador = null, $enclosure = '"')
    {
        $data = array();

        $file = fopen($arquivo, 'r');

        if ($file) {

            // Ler cabecalho do arquivo
            $header = fgetcsv($file, 0, $delimitador, $enclosure);

            // Enquanto nao terminar o arquivo
            while (!feof($file)) {

                // Ler uma linha do arquivo
                $line = fgetcsv($file, 0, $delimitador, $enclosure);
                if (!$line) {
                    continue;
                }
                $data[] = array_combine($header, $line);
            }
            fclose($file);
        }

        return $data;
    }

    /**
     * prints commands avaliable
     */
    private function showHelp()
    {
        foreach ($this->getCommands() as $key => $val) {
            $this->message($key . "\t" . $val['desc'] . "\n"
                . "\t" . $val['use'] . "\n\n");
        }
    }
};
