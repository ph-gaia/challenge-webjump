## Desafio Webjump Backend

# O teste
O desafio é desenvolver um sistema de gerenciamento de produtos. Esse sistema será composto de um cadastro de produtos e categorias. Os requisitos desse sistema estão listados nos tópicos abaixo. Não existe certo ou errado, queremos saber como você se sai em situações reais como esse desafio.

### Dependências
* PHP5 7.2
* Apache Server 2.4 +
* MySQL 5.5 +

#### Instalação
A instalação do sistema pode ser feita seguindo os seguintes passos:
> ATENÇÃO: Os passos para instalação descritos nesta documentação, assumem que a aplicação rodará em uma máquina Linux (preferencialmente Ubuntu 18.04 LTS) e que contém todas as dependências do já instaladas e configuradas.

1. Clonar ou Baixar o projeto diretamente na `Home` de usuário
```bash
$ cd ~/
```
Caso você tenha optado por baixar o arquivo zipado, descompacte o mesmo e entre no diretório criado por este processo.
```bash
$ cd ~/challenge-webjump
```
### Configurações

__Servidor HTTP__: Após a instalação do servidor, é preciso apontar o *DocumentRoot* do Apache para a pasta __public__ dentro do projeto.
Para realizar esta alteração, é preciso editar os seguintes arquivos:

Arquivo 1:
```
# nano /etc/apache2/sites-available/000-default.conf
```

Adicione a linha:
```
    <VirtualHost *:80>
        ServerName www.sandbox.webjump.com.br
        DocumentRoot /var/www/challenge-webjump/public

        <Directory /var/www/challenge-webjump/public>
            IndexOptions Charset=UTF-8
            AddDefaultCharset utf-8
            Options Indexes FollowSymLinks MultiViews
            AllowOverride All
            Order allow,deny
            allow from all
            Require all granted
        </Directory>
    </VirtualHost>
```
Arquivo 2:
```
# nano /etc/hosts
```
Adicione a seguinte linha:
```
    127.0.0.1 www.sandbox.webjump.com.br
```

__Habilitando Modo rewrite__: Esta configuração é necessária para o correto funcionamento das rotas
```
# a2enmod rewrite
```
Após as alterações, reinicie o Apache Server:
```
# service apache2 restart
```

__Informando usuário e senha do SGBD__: Agora, para que o sistema reconheça o SGBD e consiga se conectar ao mesmo é necessário fornecer as credenciais de acesso. **Antes disso não esqueça de criar um banco com o nome `webjump`**. Agora edite o arquivo *App/Config/ConfigDatabase.php* e altere conforme abaixo:
```php
    public $db = [
        'sgbd' => 'mysql',
        'server' => 'localhost', // <- informe o servidor de Banco de Dados
        'dbname' => 'webjump',
        'username' => 'root',
        'password' => 'SENHA_DEFINIDA_NA_ISTALAÇÃO_DO_MYSQL', // <- Altere aqui!
        'options' => [\PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES 'utf8'"],
    ];
```

__Criação das tabelas__
Você deve utilizar o assistente e executar o comando **setup**.
Veja mais detalhes na seção:
[Ir para seção](#assistente)

### Funcionamento
Basicamente, os acessos são recebidos pelo arquivo *public/index.php* que por sua vez inicia a aplicação. Ao instanciar a Classe *\Core\Bootstrap\InitApp()* o Roteador do sistema entra em ação para validar a requisição e rota acessada, caso tudo ocorra como o esperado, são guardados os parâmetros enviados na URI; contudo, se a Rota requisitada não estiver registrada na Classe */App/Route/Route.php*, o roteador muda o Status da requisição para __404 Not Found__ e aborta a execução do script.

Após a confirmação da rota, o Controller e o Método definido para a mesmo são validados, caso um deles não esteja em conformidade uma __\Exception(...)__ é lançada e o script abortado. Se nenhuma exceção foi lançada o método __run()__ será executado, o que instanciará o Controller da rota e a execução do método;

__Rotas__: As rotas são registradas na Classe */App/Route/Route.php* dentro do método _registra()_  com o seguinte padrão:
```php
    $this->routeMap->rotaGet(
        array('/categories/edit/{id}', 'CategoriesController@edit', array(
            'id' => '/\d+/'
        )));
```

__Controllers__: Os Controllers têm a simples função de chamar os métodos responsáveis por determinada ação no sistema. Para a criação de um novo Controller, são necessárias as seguintes observações:
1. Estar definido dentro do *namespace App\Controllers;*
2. Estender a Classe *\Core\Controller\AbstractController;*
3. Implementar a Interface *\Core\Interfaces\ControllerInterface*;

__Models__: Neste sistema, os Models são Classes que manipulam o CRUD e validam os dados enviados. Para criar uma Classe do tipo Model, são necessárias as seguintes observações:
1. Estar definido detro do *namespace App\Models;*
2. Extender a Classe *\Core\Database\ModelAbstract;*

### Endpoints
1. [GET] / -> Retorna 200 Ok (Página inicial)
2. [GET] /home -> Retorna 200 Ok (Página inicial)
3. [GET] /categories -> Retorna 200 Ok (Lista de categorias)
4. [GET] /categories/visualizar/page/{page} -> Retorna 200 Ok (Lista de categorias passando parâmetro de paginação)
5. [GET] /categories/create -> Retorna 200 Ok (Formulário de criação de categorias)
6. [GET] /categories/edit/{id} -> Retorna 200 Ok (Formulário de edição de categorias)
7. [POST] /categories/register -> Retorna objeto JSON e código 201 Ok (Adiciona uma nova categoria)
8. [POST] /categories/update -> Retorna objeto JSON e código 200 Ok (Atualiza uma categoria específica)
9. [POST] /categories/delete/id/{id} -> Retorna objeto JSON e código 200 Ok (Exclui uma categoria específica)
10. [GET] /products -> Retorna 200 Ok (Lista de produtos)
11. [GET] /products/visualizar/page/{page} -> Retorna 200 Ok (Lista de produtos passando parâmetro de paginação)

### Assistente
Foi criado um assistente para aplicação *jumper.php*.

Você pode utilizar acessando a raiz do projeto e executando o seguinte comando:
```
# php jumper.php
```
você terá o seguinte retorno:
```
# Seja bem vindo ao assistente de importação Webjump.
```
Você pode visualizar a lista de comandos utilizado o seguinte comando:
```
# php jumper.php help
```
você terá o seguinte retorno:
```
import	Importa todos os registros do arquivo 'public/assets/import.csv'. 
	    [ $ php jumper import ]

setup	Cria o banco de dados da aplicação 
	    [ $ php jumper setup ]

help  Funcionalidades disponiveis 
	  [ $ php jumper help ]
```
Para importar os dados você pode executar o seguinte comando:
```
# php jumper.php import
```
você terá o seguinte retorno:
```
> Importação de dados iniciada...
> Importação de dados finalizada com sucesso!
```
Para criar todas as tabelas do banco de dados, você pode executar o seguinte comando:
```
# php jumper.php setup
```
você terá o seguinte retorno:
```
> Banco de Dados criado com sucesso.
```

### Considerações
Como o foco principal do teste é o backend, o frontend ficou apenas com os HTML que foram passados prontos.

### Crédito
Esta aplicação foi desenvolvida por [Paulo Henrique Coelho Gaia](mailto:phcgaia11@yahoo.com.br).