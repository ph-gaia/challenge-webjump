-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------
-- -----------------------------------------------------
-- Schema webjump
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema webjump
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `webjump` DEFAULT CHARACTER SET utf8 ;
USE `webjump` ;

-- -----------------------------------------------------
-- Table `webjump`.`categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webjump`.`categories` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(90) NULL DEFAULT NULL,
  `code` VARCHAR(60) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `webjump`.`products`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webjump`.`products` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(180) NULL DEFAULT NULL,
  `sku` VARCHAR(60) NULL DEFAULT NULL,
  `price` DECIMAL(10,2) NULL DEFAULT NULL,
  `description` TEXT NULL DEFAULT NULL,
  `quantity` INT(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `webjump`.`products_has_categories`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `webjump`.`products_has_categories` (
  `products_id` INT(11) NOT NULL,
  `categories_id` INT(11) NOT NULL,
  PRIMARY KEY (`products_id`, `categories_id`),
  INDEX `fk_products_has_categories_categories1_idx` (`categories_id` ASC),
  INDEX `fk_products_has_categories_products_idx` (`products_id` ASC),
  CONSTRAINT `fk_products_has_categories_categories1`
    FOREIGN KEY (`categories_id`)
    REFERENCES `webjump`.`categories` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_products_has_categories_products`
    FOREIGN KEY (`products_id`)
    REFERENCES `webjump`.`products` (`id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
